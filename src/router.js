import Vue from "vue";
import VueSession from "vue-session";
import Router from "vue-router";
Vue.use(Router);
Vue.use(VueSession, { persist: true });

const router = new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'face-upload',
      component: () => import('../src/components/FaceUpload.vue'),
      meta: {
         isPublic: true
      }
    },
    {
      path: '/face-recognition',
      name: 'face-recognition',
      component: () => import('../src/components/FaceDetection.vue'),
      meta: {
         isPublic: true
      }
    },
  ]
});

router.beforeEach((to, from, next) => {
    if (to.name == "face-recognition" && Vue.prototype.$session.exists()) {
      next({ name: "dashboard" });
    } else if (!to.meta.isPublic && !Vue.prototype.$session.exists()) {
      // session is not exists and trying to access private page
      next({ name: "face-recognition" });
    } else {
      next();
    }
  });
  
  export default router;