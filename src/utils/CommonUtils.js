// Name: Common Utils
// Desc: Some common function should be listed here
// Created By: Asep Maulana Ismail
// Email: asepmaulanaismail@gmail.com
// Created Date: 7 October 2019

import Vue from 'vue'
import VueSweetalert2 from 'vue-sweetalert2'

Vue.use(VueSweetalert2)

export default {
  // attributes
  perPage: localStorage.perPage || 10,
  validation: {
    mandatory(v, fieldName){
      return !!v || fieldName + ' wajib diisi'
    },
    min(v, fieldName, min){
      let length = 0
      if (v)
        length = v.length
      return (length >= min) || fieldName + ' harus lebih dari ' + min + ' karakter'
    },
    max(v, fieldName, max){
      let length = 0
      if (v)
        length = v.length
      return (length <= max) || fieldName + ' harus kurang dari ' + max + ' karakter'
    },
    email(v){
      // eslint-disable-next-line
      return (!v || /.+@.+\..+/.test(v)) || 'Silakan masukkan alamat email anda dengan format: email@example.com'
    },
    internationalPhone(v, fieldName){
      // eslint-disable-next-line
      return (!v || /^\+[0-9]?()[0-9](\s|\S)(\d[0-9]{9})$/gm.test(v)) ||fieldName + ' tidak valid dengan format nomor ponsel internasional'
    },
    
  },
  
  // method
  toast(message){
    Vue.swal({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000,
      type: 'success',
      title: 'Info:',
      text: message
    });
  },
  error(message){
    Vue.swal(
      "Oops...",
      message || "Terjadi kesalahan",
      "error");
  },
  warning(message){
    Vue.swal(
      "Oops...",
      message || "Terjadi kesalahan",
      "warning");
  },
  closeSwal(){
    var elements = document.getElementsByClassName(".swal2-container");
    while(elements.length > 0){
        elements[0].parentNode.removeChild(elements[0]);
    }
  },
  getResponseMessage(value){
    if (value.data)
      return value.data.message
    else 
      return value.message || "Something went wrong"
  },
  isExtraSmall(that){
    return that.$vuetify.breakpoint.name == "xs"
  },
  isSmall(that){
    return that.$vuetify.breakpoint.name == "sm"
  },
  isMedium(that){
    return that.$vuetify.breakpoint.name == "md"
  },
  isLarge(that){
    return that.$vuetify.breakpoint.name == "lg"
  },
  isExtraLarge(that){
    return that.$vuetify.breakpoint.name == "xl"
  }
}