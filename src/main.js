import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import axios from 'axios'
import router from './router'
import VueSweetalert2 from 'vue-sweetalert2';

// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';

Vue.use(VueSweetalert2);

Vue.config.productionTip = false
Vue.config.productionTip = false

let server = process.env.VUE_APP_BASE_URL
var token = null
token = localStorage.token
axios.defaults.baseURL = server
axios.defaults.crossDomain = true
axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
axios.defaults.headers.common['Content-Type'] = 'application/json'
axios.defaults.headers.common['Access-Control-Allow-Origin'] = 'http://localhost:8080'

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
