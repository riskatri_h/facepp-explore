const FACE_HOST = "https://api-us.faceplusplus.com/"
const FACE_FACEPP = 'facepp/v3/'
const FACE_DETECT = FACE_FACEPP + "detect";
const FACE_LANDMARK = "facepp/v1/face/thousandlandmark"
// const FACE_COMPARE = FACE_FACEPP + "compare";
const FACE_SEARCH = FACE_FACEPP + "search";
// facetoken
const FACETOKEN = FACE_FACEPP + "face/";
const FACETOKEN_ANALYZE = FACETOKEN + "analyze";
// faceset
const FACESET = FACE_FACEPP + "faceset/";
const FACESET_CREATE = FACESET + "create";
const FACESET_ADDFACE = FACESET + "addface";


import axios from 'axios';
// import api from "@/apis/CommonAPI"
let headers = new Headers()
headers.append('Access-Control-Allow-Origin', 'http://localhost:8080')
headers.append("Content-Type", "multipart/form-data")
export default {
    async detectFace(form) {
        var req = await axios({
            url: FACE_HOST + FACE_DETECT,
            method: 'POST',
            data: form,
            headers: headers
        })
        return req
    },
    async analyze(form) {
        var req = await axios({
            url: FACE_HOST + FACETOKEN_ANALYZE,
            method: 'POST',
            data: form,
            headers: headers
        })
        return req
    },
    async faceSetCreate(form) {
        var url = FACE_HOST  + FACESET_CREATE;
        var req = await axios({
            url: url,
            method: 'POST',
            data: form,
            headers: headers
        })
        return req
    },
    async faceSearch(form) {
        var url = FACE_HOST  + FACE_SEARCH;
        var req = await axios({
            url: url,
            method: 'POST',
            data: form,
            headers: headers
        })
        return req
    },
    async faceAdd(form) {
        var url = FACE_HOST  + FACESET_ADDFACE;
        var req = await axios({
            url: url,
            method: 'POST',
            data: form,
            headers: headers
        })
        return req
    },
    async faceLandmark(form) {
        var url = FACE_HOST + FACE_LANDMARK
        var req = await axios({
            url: url,
            method: 'POST',
            data: form,
            headers: headers
        })
        return req
    }
}