import Vue from 'vue'
import axios from 'axios'
import VueSweetalert2 from 'vue-sweetalert2'
import utils from "@/utils/CommonUtils"

Vue.use(VueSweetalert2)

export default {
  async post(url, data, message, silent) {
    var req = await axios.post(url, data)
      .then(res => {
        if (message)
          utils.toast(message)
        return res
      }) 
      .catch(error => {
        if (error.response){
          if (error.response.data.statusCode != 404 && !silent && error.response.data.statusCode != 401  && error.response.data.statusCode != 400)
            utils.error(error.response.data.message)
          if (error.response.data.statusCode == 401){
            Vue.prototype.$session.destroy()
            if (error.response.data.message.includes("Token")){
              error.response.data.message = "Sesi anda sudah berakhir, silahkan login kembali"
            }
          }
          return error.response.data
        }else{
          utils.error(error.message)
          return error
        }
      })

    return req
  },
  async put(url, data, message) {
    var req = await axios.put(url, data)
      .then(res => {
        if (message)
          utils.toast(message)
        return res
      })
      .catch(error => {
        if (error.response){
          if (error.response.data.statusCode != 404 && error.response.data.statusCode != 401)
            utils.error(error.response.data.message)
          if (error.response.data.statusCode == 401){
            Vue.prototype.$session.destroy()
            if (error.response.data.message.includes("Token")){
              error.response.data.message = "Sesi anda sudah berakhir, silahkan login kembali"
            }
          }
          return error.response.data
        }else{
          utils.error(error.message)
          return error
        }
      })

    return req
  },
  async delete(url, data, message, silent) {
    var req = await axios.delete(url, {
        data: data
      })
      .then(res => {
        if (message)
          utils.toast(message)
        return res
      })
      .catch(error => {
        if (!silent){
          if (error.response.data.statusCode != 404 && error.response.data.statusCode != 401)
            utils.error(error.response.data.message)
        }
        if (error.response.data.statusCode == 401){
          Vue.prototype.$session.destroy()
          if (error.response.data.message.includes("Token")){
            error.response.data.message = "Sesi anda sudah berakhir, silahkan login kembali"
          }
        }
        return error.response.data
      })

    return req
  },
  async get(url, silent) {
    var req = await axios.get(url)
      .then(res => {
        return res
      })
      .catch(error => {
        if (error.response){
          if (!silent){
            if (error.response.data.statusCode != 404 && error.response.data.statusCode != 401)
              utils.error(error.response.data.message)
          }
          if (error.response.data.statusCode == 401){
            Vue.prototype.$session.destroy()
            if (error.response.data.message.includes("Token")){
              error.response.data.message = "Sesi anda sudah berakhir, silahkan login kembali"
            }
          }
          return error.response.data
        }else{
          utils.error(error.message)
          return error
        }
      })

    return req
  },
  async postMultipart(url, formData){
    var req = await axios.post(url, formData,
      {
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      })
      .then(res => {
        return res
      })
      .catch(function (error) {
        if (error.response){
          if (error.response.data.statusCode != 404 && error.response.data.statusCode != 401)
            utils.error(error.response.data.message)
          if (error.response.data.statusCode == 401){
            Vue.prototype.$session.destroy()
            if (error.response.data.message.includes("Token")){
              error.response.data.message = "Sesi anda sudah berakhir, silahkan login kembali"
            }
          }
          return error.response.data
        }else{
          utils.error(error.message)
          return error
        }
      })
    return req
  },
  async downloadExcel(url) {
    var req = await axios.get(url,
        {
            headers:
            {
                'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
            },
            responseType: 'arraybuffer',
        }
    ).then(res => {
      return res
    }).catch(function (error) {
      if (error.response){
        if (error.response.data.statusCode != 404 && error.response.data.statusCode != 401)
          utils.error(error.response.data.message)
        if (error.response.data.statusCode == 401){
          Vue.prototype.$session.destroy()
          if (error.response.data.message.includes("Token")){
            error.response.data.message = "Sesi anda sudah berakhir, silahkan login kembali"
          }
        }
        return error.response.data
      }else{
        utils.error(error.message)
        return error
      }
    })
    return req
},
async getWithData(url, data, silent) {
  console.log(data)
  var req = await axios.get(url, 
    {
      params: data
    },
    {
      headers: {
        'Content-Type': 'application/json'
      }
    })
    .then(res => {
      console.log(res)
      return res
    })
    .catch(error => {
      if (error.response){
        if (!silent){
          if (error.response.data.statusCode != 404 && error.response.data.statusCode != 401)
            utils.error(error.response.data.message)
        }
        if (error.response.data.statusCode == 401){
          Vue.prototype.$session.destroy()
          if (error.response.data.message.includes("Token")){
            error.response.data.message = "Sesi anda sudah berakhir, silahkan login kembali"
          }
        }
        return error.response.data
      }else{
        utils.error(error.message)
        return error
      }
    })

  return req
},
}