import api from "@/apis/CommonAPI"

export default {
  async post(form) {
    var req = await api.post('/api/v1/auth/login', form, {
      header: { 'Authorization': this.token }
    })
    return req
  },
}
